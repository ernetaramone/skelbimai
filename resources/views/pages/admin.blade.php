@extends('layouts/main')
@section('content')


    <table>
    <tr>
        <th>Pavadinimas</th>
        <th>Tekstas</th>
        <th>View details</th>
        <th>Edit</th>
        <th>Delete</th>
    </tr>
        @foreach($posti as $post)

            <tr>
                <td><h3>{{str_limit($post->name,20)}}</h3></td>
                <td><p>{{str_limit($post->desc,50)}} </p></td>
                <td><p><a class="btn btn-default" href="post/{{$post->id}}" role="button">View details &raquo;</a></p></td>
                <td><p><a class="btn btn-default" href="/post/{{$post->id}}/edit" role="button">Edit</a></p></td>
                <td><p><a class="btn btn-danger" href="/post/{{$post->id}}/delete" role="button">Delete</a></p></td>
            </tr>


        @endforeach

        </table>
    <hr>
    <p><a class="btn btn-danger"  href="/nauji">Naujas irasas</a></p>



@endsection