
@extends('layouts/main')
@section('content')
    <h6>Kategorija: {{$post->cat}}</h6>
    <h2>{{$post->name}}</h2>
    <p>{{$post->desc}}</p>

    <div class="comments">
        <h4>Komentarai</h4>
        <ul class="list-group">
                @foreach($post->comments as $comment)
                    <li>{{$comment->body}}</li>
                    <hr>
                @endforeach
        </ul>
    </div>

    <hr>
    <div class="card">
        <div class="card-block">
            <form action="/post/{{$post->id}}/comment" method="POST">
                {{csrf_field()}}
                <div class="form-group">
                    <textarea name="body" placeholder="Jusu komentaras" class="form-control"></textarea>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Komentuok</button>
                </div>
            </form>
        </div>
    </div>

    @if(Auth::id()==$post->user_id)
        <p><a class="btn btn-default" href="/post/{{$post->id}}/edit" role="button">Edit</a></p>
        <p><a class="btn btn-default" href="/post/{{$post->id}}/delete" role="button">Delete</a></p>
    @endif

@endsection
